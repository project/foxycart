<?php

// $Id$

?>
<h2>Your Cart</h2>
<p data-fc-id="minicart" style="display: none;">
  <a href="https://<?php echo $fc_domain; ?>/cart?cart=view">
    <span data-fc-id="minicart-quantity">0</span>
    <span data-fc-id="minicart-singular"> item </span>
    <span data-fc-id="minicart-plural"> items </span>
    total: $<span data-fc-id="minicart-order-total">0</span>
  </a>
</p>

<div id="fc_minicart_empty" style="display: none">Your shopping cart is empty</div>
